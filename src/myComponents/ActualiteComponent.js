// import axios from "axios";
import React, { useEffect, useState } from "react";

// @mui material components
import Grid from "@mui/material/Grid";

// Material Dashboard 2 React components
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";

// Material Dashboard 2 React example components
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import Footer from "examples/Footer";
// import MDButton from "components/MDButton";
import Header from "layouts/profile/components/Header";
import DefaultProjectCard from "examples/Cards/ProjectCards/DefaultProjectCard";

import homeDecor1 from "assets/images/home-decor-1.jpg";
import axios from "axios";
import slugify from "slugify";

function ActualiteComponent() {

    const [articles, setArticles] = useState([]);
    function getArticle() {
        const url = `http://localhost:8080/articles/`;
        axios.get(url).then((res) => {
            setArticles(res.data);
            console.log(articles);
        })
    }

    document.title = "ACTUALITE";
    useEffect(() => {
        getArticle();
        // console.log(article);
    }, [])
    return (
        <DashboardLayout>
            <DashboardNavbar />
            <MDBox pt={6} pb={3}>
                <Header>
                    <MDBox pt={2} px={2} lineHeight={1.25}>
                        <MDTypography variant="h6" fontWeight="medium">
                            ARTICLES
                        </MDTypography>
                        <MDBox mb={1}>
                            <MDTypography variant="button" color="text">
                                Actualite
                            </MDTypography>
                        </MDBox>
                    </MDBox>
                    <MDBox p={2}>
                        <Grid container spacing={6}>

                            {articles.map((article) => (
                                <Grid item xs={12} md={6} xl={3}>
                                    <DefaultProjectCard
                                        image={homeDecor1}
                                        label={`#Article-${article.id}`}
                                        title={article.titre}
                                        description={article.resume}
                                        action={{
                                            type: "internal",
                                            route: `/${slugify(article.titre)}/${article.id}`,
                                            // route: `/tables`,
                                            color: "info",
                                            label: "view article",
                                        }}
                                    />
                                </Grid>
                            ))}

                        </Grid>
                    </MDBox>
                </Header>
            </MDBox>
            <Footer />
        </DashboardLayout>
    );
}

export default ActualiteComponent;