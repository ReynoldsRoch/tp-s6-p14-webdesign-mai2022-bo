import React from 'react';
// import { makeStyles } from '@mui/core/styles';
// prop-types is a library for typechecking of props
// import PropTypes from "prop-types";

// @mui material components
// import Icon from "@mui/material/Icon";

// Material Dashboard 2 React components
import MDBox from "components/MDBox";

// Material Dashboard 2 React contexts
import { useMaterialUIController } from "context";

function MyArrayComponent() {

    const [controller] = useMaterialUIController();
    const { darkMode } = controller;

    const header = ['Author', 'Article', 'DatePublication', 'Action'];

    return (
        <div>
            <MDBox
                component="table"
                width={1}
                sx={({ palette: { light }, borders: { borderWidth } }) => ({
                    borderCollapse: "collapse",
                    borderSpacing: 0,
                    borderBottom: `${borderWidth[1]} solid ${light.main}`,
                })}
            >
                <MDBox component="thead">
                    <MDBox component="tr">
                        {header.map((item) => (
                            <MDBox
                                // key={}
                                component="th"
                                width={100 / header.length}
                                py={1.5}
                                px={3}
                                sx={({ palette: { light }, borders: { borderWidth } }) => ({
                                    borderBottom: `${borderWidth[1]} solid ${light.main}`,
                                })}
                            >
                                <MDBox
                                    position="relative"
                                    textAlign="center"
                                    color={darkMode ? "white" : "secondary"}
                                    opacity={0.7}
                                    sx={({ typography: { size, fontWeightBold }, breakpoints }) => ({
                                        fontSize: size.xxs,
                                        fontWeight: fontWeightBold,
                                        textTransform: "uppercase",
                                        cursor: "pointer",
                                        userSelect: "none",
                                        [breakpoints.down("sm")]: {
                                            fontSize: size.xs,
                                        },
                                    })}
                                >
                                    {item}
                                </MDBox>
                            </MDBox>
                        ))}
                    </MDBox>
                </MDBox>
            </MDBox>
        </div>
    );
};


export default MyArrayComponent;
