/**
=========================================================
* Material Dashboard 2 React - v2.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2022 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/

// react-router-dom components
import { Link } from "react-router-dom";

// @mui material components
import Card from "@mui/material/Card";
// import Checkbox from "@mui/material/Checkbox";

// Material Dashboard 2 React components
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";
import MDInput from "components/MDInput";
import MDButton from "components/MDButton";

// Authentication layout components
import CoverLayout from "layouts/authentication/components/CoverLayout";

// Images
import bgImage from "assets/images/bg-sign-up-cover.jpeg";
import axios from "axios";
import { useState } from "react";
import MDAlert from "components/MDAlert";

function Cover() {
  document.title = "SIGN-UP";

  const [alert, setAlert] = useState(2);

  const signup = async () => {
    console.log("signup");
    const nom = document.getElementById("nom").value;
    const prenom = document.getElementById("prenom").value;
    const email = document.getElementById("email").value;
    const password = document.getElementById("password").value;
    const dtn = document.getElementById("dtn").value;

    const url = `http://localhost:8080/clients?email=${email}&password=${password}&nom=${nom}&prenom=${prenom}&dtn=${dtn}`;

    axios.post(url).then((response) => {
      // console.log(response.data);
      setAlert(response.data);
    });

    // console.log(url);
  };

  return (
    <CoverLayout image={bgImage}>
      <Card>
        <MDBox
          variant="gradient"
          bgColor="info"
          borderRadius="lg"
          coloredShadow="success"
          mx={2}
          mt={-3}
          p={3}
          mb={1}
          textAlign="center"
        >
          <MDTypography variant="h4" fontWeight="medium" color="white" mt={1}>
            Join us today
          </MDTypography>
          <MDTypography display="block" variant="button" color="white" my={1}>
            Enter your informations to register
          </MDTypography>
        </MDBox>
        <MDBox pt={4} pb={3} px={3}>
          <MDBox component="form" role="form">
            <MDBox mb={2}>
              <MDInput
                type="text"
                id="nom"
                defaultValue="Phoenix"
                label="Nom"
                variant="standard"
                fullWidth
                required
              />
            </MDBox>
            <MDBox mb={2}>
              <MDInput
                type="text"
                defaultValue="Paul"
                label="Prenom"
                id="prenom"
                variant="standard"
                fullWidth
                required
              />
            </MDBox>
            <MDBox mb={2}>
              <MDInput
                type="date"
                defaultValue="2002-01-01"
                label="DateNaissance"
                id="dtn"
                variant="standard"
                fullWidth
                required
              />
            </MDBox>
            <MDBox mb={2}>
              <MDInput
                type="email"
                defaultValue="paul.phoenix@gmail.com"
                label="Email"
                id="email"
                variant="standard"
                fullWidth
                required
              />
            </MDBox>
            <MDBox mb={2}>
              <MDInput
                type="password"
                defaultValue="tekken7"
                label="Password"
                id="password"
                variant="standard"
                fullWidth
                required
              />
            </MDBox>
            <MDBox mt={4} mb={1}>
              <MDButton onClick={signup} variant="gradient" color="info" fullWidth>
                sign in
              </MDButton>
            </MDBox>
            <MDBox mt={3} mb={1} textAlign="center">
              <MDTypography variant="button" color="text">
                Already have an account?{" "}
                <MDTypography
                  component={Link}
                  to="/"
                  variant="button"
                  color="info"
                  fontWeight="medium"
                  textGradient
                >
                  Sign In
                </MDTypography>
              </MDTypography>
              {alert === 0 ? (
                <div>
                  <MDAlert color="error">Inscription Incorrect!</MDAlert>
                </div>
              ) : null}
              {alert === 1 ? (
                <div>
                  <MDAlert color="success">Inscription Terminée!</MDAlert>
                </div>
              ) : null}
            </MDBox>
          </MDBox>
        </MDBox>
      </Card>
    </CoverLayout>
  );
}

export default Cover;
