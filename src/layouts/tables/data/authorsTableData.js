/* eslint-disable react/prop-types */
/* eslint-disable react/function-component-definition */
/**
=========================================================
* Material Dashboard 2 React - v2.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2022 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/

// Material Dashboard 2 React components
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";
import MDAvatar from "components/MDAvatar";
import MDBadge from "components/MDBadge";

// Images
// import team2 from "assets/images/team-2.jpg";
// import team3 from "assets/images/team-3.jpg";
// import team4 from "assets/images/team-4.jpg";
import { useEffect, useState } from "react";
import axios from "axios";
import { NavLink } from "react-router-dom";
import slugify from "slugify";

export default function data() {

  document.title = "ARTICLE INVALID";

  const Author = ({ image, name, email }) => (
    <MDBox display="flex" alignItems="center" lineHeight={1}>
      <MDAvatar src={image} name={name} size="sm" />
      <MDBox ml={2} lineHeight={1}>
        <MDTypography display="block" variant="button" fontWeight="medium">
          {name}
        </MDTypography>
        <MDTypography variant="caption">{email}</MDTypography>
      </MDBox>
    </MDBox>
  );

  const Job = ({ title, description }) => (
    <MDBox lineHeight={1} textAlign="left">
      <MDTypography display="block" variant="caption" color="text" fontWeight="medium">
        {title}
      </MDTypography>
      <MDTypography variant="caption">{description}</MDTypography>
    </MDBox>
  );

  // modif
  const [articleClient, setArticleClient] = useState([]);

  function getArticleClient() {
    const url = "http://localhost:8080/articleNonValide";
    axios.get(url).then((res) => {
      setArticleClient(res.data);
    })
  }

  useEffect(() => {
    getArticleClient();
    console.log(articleClient);
  }, [])

  // 

  return {
    columns: [
      { Header: "author", accessor: "author", width: "45%", align: "left" },
      { Header: "article", accessor: "function", align: "left" },
      { Header: "datepublication", accessor: "employed", align: "center" },
      { Header: "action", accessor: "action", align: "center" },
    ],

    rows: articleClient.map((article) => (
      {
        author: <Author name={`${article.client.nom} ${article.client.prenom}`} email={article.client.mail} />,
        function: <Job title={article.titre} description={article.resume} />,
        employed: (
          <MDTypography variant="caption" color="text" fontWeight="medium">
            {article.datePublication}
          </MDTypography>
        ),
        action: (
          <NavLink to={`/${slugify(article.titre)}/${article.id}`} className="nav-link">
            <MDBadge badgeContent="VIEW" variant="contained" container />
          </NavLink>

        ),
      }
    )),
  };
}
